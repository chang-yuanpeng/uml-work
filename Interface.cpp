#include<iostream>
using namespace std;
//接口多态 
class Base{
	public :
	virtual void Speak()=0; 
}; 
class Derived0 :public Base{
	public :
	void Speak(){
		cout<<"Im Derived0"<<endl;
	}
};
class Derived1 : public Base{
	public :
	void Speak(){
		cout<<"Im Derived1"<<endl;
	}
};
class Application{
	private:
		Base * bp;
	public: void Set(Base*p){
		bp=p;
	}
	void Func(){
		bp->Speak();
	}
}; 
int main(){
	Application app;
	app.Set(new Derived0());
	app.Func();
	app.Set(new Derived1());
	app.Func();
	return 0;
} 