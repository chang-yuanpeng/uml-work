
public abstract class Animal {
	private String name;
	public Animal(String s) {
		name = s;
	}
	public  void SetName(String s) {
		name=s;
	}
	public String GetName() {
		return name;
	}
	public abstract void AnimalCall();
	public abstract void AnimalAct();
}
class Dog extends Animal{
	
	public Dog(String s) {
		super(s);
	}
	public void AnimalCall() {
		System.out.println( this.GetName() +" 汪汪");
	}
	public void AnimalAct() {
		System.out.println(this.GetName()+" 吐舌");
	}
}
class Cat extends Animal{
	public Cat(String s) {
		super(s);
	}
	public void AnimalCall() {
		System.out.println(this.GetName()+" 喵喵");
	}
	public void AnimalAct() {
		System.out.println(this.GetName()+" 打滚");
	}
}
class Main{
public static void main(String argc[]) {
	Animal dog = new Dog("Spike");
	Animal cat = new Cat("Tom"); 
	dog.AnimalAct();
	cat.AnimalCall();
	dog.AnimalCall();
	cat.AnimalAct();
	}
}