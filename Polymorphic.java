class A{
    private void a(){
        System.out.println("A.a");
    }
    private void b(){
        System.out.println("A.b");
    }
    public void ab(){
        a();
        b();
    }
}
public class B extends A{
    private void a(){
        System.out.println("B.a");
    }
    protected void b(){
        System.out.println("B.b");
    }
    
    public static void main(String args[]){
        new B().ab();
    }
}